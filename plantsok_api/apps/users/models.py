import uuid

from django.contrib.auth.models import AbstractUser
from django.core.validators import EmailValidator
from django.db import models

from django.utils.translation import gettext_lazy as _


class User(AbstractUser):
    """ User model. """

    email_validator = EmailValidator()

    email = models.EmailField(
        _('email address'),
        blank=False,
        unique=True,
        validators=[email_validator],
        error_messages={
            'unique': _("A user with that email already exists."),
        },
    )

    is_active = models.BooleanField(
        _('active'),
        default=False,
        help_text=_(
            'Designates whether this user should be treated as active. '
            'Unselect this instead of deleting accounts.',
        ),
    )

    def __str__(self):
        return self.username


class ConfirmToken(models.Model):
    """ Generate token for user confirmation. """

    token = models.UUIDField(
        primary_key=True,
        default=uuid.uuid4,
        editable=False,
    )
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=False)
