from celery import shared_task
from django.core.mail import send_mail
from django.template.loader import render_to_string

from plantsok_api import settings


@shared_task
def send_email(email, token):
    """ Send email after registration. """

    subject = 'Thank you for registering to our site.'
    message = render_to_string('user_verification.html', {'token': token})
    email_from = settings.EMAIL_HOST_USER
    recipient_list = [email]
    send_mail(subject, message, email_from, recipient_list)
    print(f"Registration email was successfully submitted to {email}")
