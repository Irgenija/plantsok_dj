from rest_framework import generics, permissions, status
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from plantsok_api.apps.core.views import AllowedMethodsMixin
from plantsok_api.apps.users.models import User, ConfirmToken
from plantsok_api.apps.users.serializers import (
    UserRegisterSerializer, UserSerializer)
from plantsok_api.apps.users.tasks import send_email


class UserListCreateView(generics.ListCreateAPIView):
    """ List of users and create user. """

    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = [IsAuthenticated]

    def get_permissions(self):
        if self.request.method == "POST":
            return [permissions.AllowAny()]

        return [permissions.AllowAny()]

    def get_serializer_class(self):
        if self.request.method == "POST":
            return UserRegisterSerializer

        return self.serializer_class

    def post(self, request, *args, **kwargs):
        data = request.data
        email = data.get('email')
        create_serializer = UserRegisterSerializer(data=data)
        create_serializer.is_valid(raise_exception=True)
        self.perform_create(create_serializer)
        headers = self.get_success_headers(create_serializer.data)

        confirm_token = ConfirmToken.objects.create(
            user_id=create_serializer.data.get('id'),
        )

        send_email.delay(email, confirm_token.token)
        return Response(
            create_serializer.data,
            status=status.HTTP_201_CREATED,
            headers=headers,
        )


class ActivationView(APIView):
    """ Activate user account. """

    def get(self, request, *args, **kwargs):
        token = self.kwargs.pop("token")

        if not token:
            return Response(
                status=status.HTTP_400_BAD_REQUEST,
                data={"Error": "Token was not provided."},
            )
        confirm_token = ConfirmToken.objects.filter(token=token).first()

        if not confirm_token:
            return Response(
                status=status.HTTP_400_BAD_REQUEST,
                data={"Error": "User token does not exist."},
            )

        if confirm_token.user and confirm_token.user.is_active:
            return Response(
                status=status.HTTP_400_BAD_REQUEST,
                data={"Error": "User is already activated."},
            )

        confirm_token.user.is_active = True
        confirm_token.user.save()

        return Response(status=status.HTTP_200_OK)


class UserDetailView(
    AllowedMethodsMixin,
    generics.RetrieveUpdateDestroyAPIView,
):
    """ Retrieve, update or delete user. """

    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = [IsAuthenticated]
