from django.urls import path

from plantsok_api.apps.users.views import (
    UserDetailView, UserListCreateView, ActivationView,
)

urlpatterns = [
    path('', UserListCreateView.as_view(), name='users'),
    path('<int:pk>/', UserDetailView.as_view(), name='user_details'),
    path(
        'validate/<token>/',
        ActivationView.as_view(),
        name='user_activation',
    ),
]
