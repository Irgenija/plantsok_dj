from django.apps import AppConfig


class UsersConfig(AppConfig):
    name = 'plantsok_api.apps.users'
