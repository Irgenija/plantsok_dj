from rest_framework import generics, filters
from rest_framework.parsers import MultiPartParser
from rest_framework.permissions import IsAuthenticated

from plantsok_api.apps.core.views import AllowedMethodsMixin
from plantsok_api.apps.plants.models import Plant, Photo
from plantsok_api.apps.plants.serializers import (
    PlantSerializer, PlantDetailSerializer,
    ListOfUsersPlantsSerializer, PhotoSerializer, PlantCreateSerializer,
)


class PlantListCreateView(generics.ListCreateAPIView):
    """ List of plants and create plant. """

    queryset = Plant.objects.all()
    serializer_class = PlantSerializer
    permission_classes = [IsAuthenticated]
    filter_backends = (filters.SearchFilter,)
    search_fields = ('name', 'seller', 'note')

    def get_serializer_class(self):
        if self.request.method == "POST":
            return PlantCreateSerializer

        return self.serializer_class


class PlantDetail(
    AllowedMethodsMixin,
    generics.RetrieveUpdateDestroyAPIView,
):
    """ Retrieve, update or delete plant. """

    queryset = Plant.objects.all()
    serializer_class = PlantDetailSerializer
    permission_classes = [IsAuthenticated]


class ListOfUsersPlants(generics.ListAPIView):
    """ List of user's plants. """

    queryset = Plant.objects.all()
    serializer_class = ListOfUsersPlantsSerializer
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        return self.queryset.filter(user_id=self.kwargs['user_id']).all()


class PhotosListCreateView(generics.ListCreateAPIView):
    """ List of photos, added and deleted photos. """

    queryset = Photo.objects.all()
    serializer_class = PhotoSerializer
    parser_classes = [MultiPartParser]
    permission_classes = [IsAuthenticated]


class PhotosDeleteView(generics.DestroyAPIView):
    """ Deleted photos. """

    queryset = Photo.objects.all()
    permission_classes = [IsAuthenticated]
