from rest_framework import serializers
from rest_framework.fields import ImageField, CharField

from plantsok_api.apps.plants.models import Plant, Photo


class PlantSerializer(serializers.ModelSerializer):
    """ Plant information. """

    class Meta:
        model = Plant
        fields = (
            'id',
            'name',
            'size',
            'user_id',
        )


class PhotoSerializer(serializers.ModelSerializer):
    """ Photo upload. """

    image = ImageField()

    class Meta:
        model = Photo
        fields = (
            'id',
            'image',
            'plant',
        )


class PlantCreateSerializer(serializers.ModelSerializer):
    """ Plant detail information. """

    id = CharField(read_only=True)
    user = serializers.HiddenField(
        default=serializers.CurrentUserDefault(),
    )

    class Meta:
        model = Plant
        fields = (
            'id',
            'name',
            'type',
            'size',
            'purchase_date',
            'seller',
            'note',
            'user',
        )


class PlantDetailSerializer(PlantCreateSerializer):
    """ Plant detail information. """

    photos = PhotoSerializer(
        source='images',
        many=True,
    )

    class Meta:
        model = Plant
        fields = (
            'id',
            'name',
            'type',
            'size',
            'purchase_date',
            'seller',
            'note',
            'photos',
            'user',
        )


class ListOfUsersPlantsSerializer(serializers.ModelSerializer):
    """ List of users posts serializer. """

    class Meta:
        model = Plant
        fields = (
            'name',
            'size',
        )
