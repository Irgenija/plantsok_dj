from django.apps import AppConfig


class PlantsConfig(AppConfig):
    name = 'plantsok_api.apps.plants'
