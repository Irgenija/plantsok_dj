import os
from pathlib import Path

from django.core.files.uploadedfile import SimpleUploadedFile
from rest_framework import status
from rest_framework.reverse import reverse
from rest_framework.test import APITestCase

from plantsok_api.apps.core.tests import BaseUserTest
from plantsok_api.apps.plants.models import Plant


class APIPlantsTest(BaseUserTest, APITestCase):
    """ Plants tests. """

    fixtures = ("users", "plants", "photos")

    def test_get_plants_list(self):
        response = self.client.get(
            reverse('plants'),
            HTTP_AUTHORIZATION=f"Bearer {self.token}",
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.json().get('count'), 2)

    def test_get_empty_plants_list(self):
        Plant.objects.all().delete()
        response = self.client.get(
            reverse('plants'),
            HTTP_AUTHORIZATION=f"Bearer {self.token}",
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.json().get('count'), 0)

    def test_get_plant_details(self):
        response = self.client.get(
            reverse('plant_details', args=[1]),
            HTTP_AUTHORIZATION=f"Bearer {self.token}",
        )

        expected = {
            'id': '1',
            'name': 'Name 1',
            'type': 'Type 1',
            'size': '1.7',
            'purchase_date': '2021-01-17T23:31:00.389000+02:00',
            'seller': 'Seller 1',
            'note': 'Test note',
            'photos': [
                {
                    'id': 1,
                    'image': 'http://testserver/media/Name_1.jpg',
                    'plant': 1,
                },
                {
                    'id': 2,
                    'image': 'http://testserver/media/Name_2.jpg',
                    'plant': 1,
                },
            ],
        }
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertDictEqual(expected, response.json())

    def test_get_not_existed_plant_details(self):
        response = self.client.get(
            reverse('plant_details', args=[4]),
            HTTP_AUTHORIZATION=f"Bearer {self.token}",
        )
        expected = {'detail': 'Not found.'}
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        self.assertDictEqual(expected, response.json())

    def test_create_plant(self):
        data = {
            'name': 'Name 3',
            'type': 'Type 3',
            'size': '1.7',
            'purchase_date': '2021-01-17T23:31:00.389000+02:00',
            'seller': 'Seller 3',
            'note': 'Test note',
        }

        response = self.client.post(
            reverse('plants'),
            data=data,
            HTTP_AUTHORIZATION=f"Bearer {self.token}",
            format="json",
        )
        expected = {
            'id': '3',
            'name': 'Name 3',
            'type': 'Type 3',
            'size': '1.7',
            'purchase_date': '2021-01-17T23:31:00.389000+02:00',
            'seller': 'Seller 3',
            'note': 'Test note',
        }
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertDictEqual(expected, response.json())

    def test_create_plant_without_required(self):
        data = {
            'name': '',
            'type': 'Type 3',
            'size': '1.7',
            'purchase_date': '2021-01-17T23:31:00.389000+02:00',
            'seller': 'Seller 3',
            'note': 'Test note',
            'user': 1,
        }

        response = self.client.post(
            reverse('plants'),
            data=data,
            HTTP_AUTHORIZATION=f"Bearer {self.token}",
            format="json",
        )
        expected = {'name': ['This field may not be blank.']}
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertDictEqual(expected, response.json())

    def test_update_plant(self):
        data = {
            'name': 'Test update 3',
        }

        response = self.client.patch(
            reverse('plant_details', args=[1]),
            data=data,
            HTTP_AUTHORIZATION=f"Bearer {self.token}",
        )
        expected = {
            'id': '1',
            'name': 'Test update 3',
            'type': 'Type 1',
            'size': '1.7',
            'purchase_date': '2021-01-17T23:31:00.389000+02:00',
            'seller': 'Seller 1',
            'note': 'Test note',
            'photos': [
                {
                    'id': 1,
                    'image': 'http://testserver/media/Name_1.jpg',
                    'plant': 1,
                },
                {
                    'id': 2,
                    'image': 'http://testserver/media/Name_2.jpg',
                    'plant': 1,
                },
            ],
        }

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertDictEqual(expected, response.json())

    def test_update_plant_validation(self):
        data = {
            'name': '',
        }

        response = self.client.patch(
            reverse('plant_details', args=[1]),
            data=data,
            HTTP_AUTHORIZATION=f"Bearer {self.token}",
        )
        expected = {'name': ['This field may not be blank.']}
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertDictEqual(expected, response.json())

    def test_update_plant_not_existed(self):
        data = {
            'name': "Test 1 1",
        }

        response = self.client.patch(
            reverse('plant_details', args=[5]),
            data=data,
            HTTP_AUTHORIZATION=f"Bearer {self.token}",
        )
        expected = {'detail': 'Not found.'}
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        self.assertDictEqual(expected, response.json())

    def test_delete_plant(self):
        response = self.client.delete(
            reverse('plant_details', args=[1]),
            HTTP_AUTHORIZATION=f"Bearer {self.token}",
        )
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_delete_plant_not_existed(self):
        response = self.client.delete(
            reverse('plant_details', args=[5]),
            HTTP_AUTHORIZATION=f"Bearer {self.token}",
        )
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_get_photos_list(self):
        response = self.client.get(
            reverse('gallery'),
            HTTP_AUTHORIZATION=f"Bearer {self.token}",
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.json().get('count'), 2)

    def test_upload_photo(self):
        path_img = str(Path(__file__).resolve().parents[4]) + \
            "/media/tests/test.jpg"
        image = SimpleUploadedFile(
            name='test.jpg',
            content=open(path_img, 'rb').read(),
            content_type='image/jpeg',
        )
        data = {
            'image': image,
            'plant': '2',
        }

        response = self.client.post(
            reverse('gallery'),
            data=data,
            HTTP_AUTHORIZATION=f"Bearer {self.token}",
            format='multipart',
        )
        path_uploaded_file = str(Path(__file__).resolve().parents[4]) + \
            "/media/gallery/test.jpg"
        self.assertEqual(os.path.exists(path_uploaded_file), True)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_delete_photo(self):
        response = self.client.delete(
            reverse('photo_delete', args=[1]),
            HTTP_AUTHORIZATION=f"Bearer {self.token}",
        )
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_delete_photo_not_existed(self):
        response = self.client.delete(
            reverse('photo_delete', args=[5]),
            HTTP_AUTHORIZATION=f"Bearer {self.token}",
        )
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_unauthorized_get_users_plants_list(self):
        response = self.client.get(reverse('user_plants', args=[1]))
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_get_users_plants_list(self):
        response = self.client.get(
            reverse('user_plants', args=[1]),
            HTTP_AUTHORIZATION=f"Bearer {self.token}",
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.json().get('count'), 2)

    def test_get_empty_users_plants_list(self):
        Plant.objects.all().delete()
        response = self.client.get(
            reverse('user_plants', args=[1]),
            HTTP_AUTHORIZATION=f"Bearer {self.token}",
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.json().get('count'), 0)
