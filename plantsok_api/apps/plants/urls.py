from django.urls import path

from plantsok_api.apps.plants.views import PlantDetail, PlantListCreateView, \
    ListOfUsersPlants, PhotosListCreateView, PhotosDeleteView

urlpatterns = [
    path('', PlantListCreateView.as_view(), name='plants'),
    path('<int:pk>/', PlantDetail.as_view(), name='plant_details'),
    path(
        'user/<int:user_id>/',
        ListOfUsersPlants.as_view(),
        name='user_plants',
    ),
    path('gallery/', PhotosListCreateView.as_view(), name='gallery'),
    path(
        'gallery/photo/<int:pk>/',
        PhotosDeleteView.as_view(),
        name='photo_delete',
    ),
]
