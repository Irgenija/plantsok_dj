from django.db import models
from django.utils import timezone
from plantsok_api.apps.users.models import User


class Plant(models.Model):
    """ Plant model. """

    name = models.CharField(
        max_length=128,
        help_text="Enter name of plant",
        blank=False,
        null=False,
    )
    type = models.CharField(max_length=64, help_text="Enter type of plant")
    size = models.CharField(max_length=64, help_text="Enter size of plant")
    purchase_date = models.DateTimeField(default=timezone.now, blank=True)
    seller = models.CharField(max_length=200, help_text="Enter name of seller")
    note = models.TextField(
        blank=True,
        null=True,
        help_text="Enter your comment",
    )
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=False)


class Photo(models.Model):
    """ Photos model.  """

    image = models.ImageField(upload_to='gallery')
    plant = models.ForeignKey(
        Plant,
        on_delete=models.CASCADE,
        related_name='images',
    )
